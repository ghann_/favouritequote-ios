//
//  DetailQuoteListViewController.swift
//  FavouriteQuotes
//
//  Created by unrealBots on 9/11/17.
//  Copyright © 2017 Ghani. All rights reserved.
//

import UIKit
import RealmSwift

class DetailQuoteListViewController: UIViewController {
    var categoryToBePassed:String = ""
    
    dynamic var quote: QuoteModel!
    lazy var quotes: Results<QuoteModel> = { self.realm.objects(QuoteModel.self)}()
    let realm = try! Realm()
    
    var quoteText:[String] = []
    var quoteAuthorText:[String] = []
    
    func setUpNavigation(){
        self.navigationController?.navigationBar.topItem?.title = "Back"
        self.title = categoryToBePassed
    }
    
    func getDataFromRealm(){
        let quotes = self.realm.objects(QuoteModel.self)
        quoteText = quotes.filter("category = '\(categoryToBePassed.lowercased())'").value(forKey: "text") as! [String]
        quoteAuthorText = quotes.filter("category = '\(categoryToBePassed.lowercased())'").value(forKey: "person") as! [String]
    }
}


// MARK: - Life Cycle
extension DetailQuoteListViewController{
    override func viewDidLoad() {
        
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        setUpNavigation()
        getDataFromRealm()
    }
}

// MARK: - TableView Data Source
extension DetailQuoteListViewController: UITableViewDataSource{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return quoteText.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CellQuoteDetail", for: indexPath) as! DetailQuoteListTableViewCell
        cell.lblNumberOfQuote.text = "#\(indexPath.row + 1)"
        cell.lblQuoteText.text = quoteText[indexPath.row]
        cell.lblQuoteAuthor.text = quoteAuthorText[indexPath.row]
        cell.selectionStyle = .none
        
        return cell
    }
}
