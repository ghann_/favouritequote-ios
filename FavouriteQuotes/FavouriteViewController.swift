//
//  ViewController.swift
//  FavouriteQuotes
//
//  Created by unrealBots on 9/11/17.
//  Copyright © 2017 Ghani. All rights reserved.
//

import UIKit
import SwiftyJSON
import Alamofire

class FavouriteViewController: UIViewController {
    var arrayOfQuote:[QuoteModel] = []
    
    var quoteText = ""
    var quoteAuthorText = ""

    @IBOutlet weak var tableView: UITableView!
    
    func openMenu(){
        if let container = self.so_containerViewController {
            container.isSideViewControllerPresented = true
        }
    }

    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "goToFavouriteDetail"{
            if let nav = segue.destination as? DetailFavouriteViewController{
                nav.quoteText = quoteText
                nav.quoteAuthorText = quoteAuthorText
            }
        }
    }
}


// MARK: - Life Cycle
extension FavouriteViewController{
    override func viewDidLoad() {
               
        let button = UIButton.init(type: .custom)
        button.setImage(UIImage.init(named: "menu"), for: UIControlState.normal)
        button.addTarget(self, action:#selector(FavouriteViewController.openMenu), for: UIControlEvents.touchUpInside)
        button.frame = CGRect.init(x: 0, y: 0, width:24, height: 20)
        let barButton = UIBarButtonItem.init(customView: button)
        self.navigationItem.leftBarButtonItem = barButton
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
    }
}

// MARK: - TableView Data Source
extension FavouriteViewController: UITableViewDataSource{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrayOfQuote.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "FavouriteCell", for: indexPath) as! FavouriteTableViewCell
        //cell.lblQuote.text = arrayOfQuote[indexPath.row].text
        //cell.lblQuoteAuthor.text = arrayOfQuote[indexPath.row].person
        return cell
    }
}

// MARK: - TableView Delegate
extension FavouriteViewController: UITableViewDelegate{
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let indexPath = tableView.indexPathForSelectedRow
        let currentCell = tableView.cellForRow(at: indexPath!) as! FavouriteTableViewCell
        
        quoteText = currentCell.lblQuote.text!
        quoteAuthorText = currentCell.lblQuoteAuthor.text!
        performSegue(withIdentifier: "goToFavouriteDetail", sender: self)
    }
    
}
