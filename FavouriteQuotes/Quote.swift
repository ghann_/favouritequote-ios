//
//  UserModel.swift
//  FavouriteQuotes
//
//  Created by unrealBots on 9/14/17.
//  Copyright © 2017 Ghani. All rights reserved.
//

import UIKit
import RealmSwift
import Alamofire
import SwiftyJSON


let notificationKey = "com.unrealBots"
let notificationCenter:NotificationCenter = NotificationCenter.default

class QuoteModel: Object{
    dynamic var id = 0
    dynamic var category = ""
    dynamic var person = ""
    dynamic var text = ""
    dynamic var icon = ""
}

struct RealmDatabase{
    static var sharedInstance = RealmDatabase()
    var realm: Realm!
    
    init() {
        self.realm = try! Realm()
    }
   
    func fetchQuoteData(completed: @escaping () -> ()){
        Alamofire.request("http://127.0.0.1").responseJSON { response in
            let json = JSON(response.data!)
            
            for i in 0..<json.count{
                for j in 0..<json[i]["quotes"].count{
                    
                    try! self.realm.write() {
                        let newQuote = QuoteModel()
                        newQuote.id       = json[i]["quotes"][j]["id"].int ?? 0
                        newQuote.category = json[i]["category"].string ?? ""
                        newQuote.person   = json[i]["quotes"][j]["person"].string ?? ""
                        newQuote.text     = json[i]["quotes"][j]["text"].string ?? ""
                        newQuote.icon     = json[i]["icon"].string ?? ""
                        self.realm.add(newQuote)
                    }
                }
            }
            
            var categorizedQuote: (distinct: [String], totalEach: [Int]) = (distinct: [""], totalEach: [0])
            var tempQuoteEachCategory:[Int] = []
            
            categorizedQuote.distinct = Array((Set(self.realm.objects(QuoteModel.self).value(forKey: "category") as! [String])))
            for i in 0..<categorizedQuote.distinct.count{
                let selected = self.realm.objects(QuoteModel.self).filter("category = '\(categorizedQuote.distinct[i])'").value(forKey: "text") as! [String]
                tempQuoteEachCategory.append(selected.count)
            }
            categorizedQuote.totalEach = tempQuoteEachCategory
            QuoteListViewController.sharedCat = categorizedQuote
            
            completed()
        }
    }
}





