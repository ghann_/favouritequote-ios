//
//  QuoteListViewController.swift
//  FavouriteQuotes
//
//  Created by unrealBots on 9/11/17.
//  Copyright © 2017 Ghani. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import RealmSwift

class QuoteListViewController: UIViewController {
    @IBOutlet var tableView: UITableView!
    
    static var sharedCat: (distinct: [String], totalEach: [Int]) = (distinct: [""], totalEach: [0])
    var categorizedQuote: (distinct: [String], totalEach: [Int]) = (distinct: [""], totalEach: [0])
    
    var categoryToBePassing:String = ""
    let realmDatabase = RealmDatabase.sharedInstance
    
    func openMenu(){
        if let container = self.so_containerViewController  {
            container.isSideViewControllerPresented = true
        }
    }
    func setUpBarButton(){
        let button = UIButton.init(type: .custom)
        button.setImage(UIImage.init(named: "menu"), for: UIControlState.normal)
        button.addTarget(self, action:#selector(FavouriteViewController.openMenu), for: UIControlEvents.touchUpInside)
        button.frame = CGRect.init(x: 0, y: 0, width:24, height: 20)
        let barButton = UIBarButtonItem.init(customView: button)
        self.navigationItem.leftBarButtonItem = barButton
    }
    func getData(){
        realmDatabase.fetchQuoteData {
            DispatchQueue.main.async{
                self.categorizedQuote = QuoteListViewController.sharedCat
                self.tableView.reloadData()
            }
        }
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "goToListOfSelectedQuote" {
            if let nav = segue.destination as? DetailQuoteListViewController{
                nav.categoryToBePassed = categoryToBePassing
            }
        }
    }
}


// MARK: - Life Cycle
extension QuoteListViewController{
    override func viewDidLoad() {
        setUpBarButton()
        getData()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        if let container = self.so_containerViewController {
            container.isSideViewControllerPresented = false
        }
        tableView.tableFooterView = UIView()
    }
}

// MARK: - TableView Data Source
extension QuoteListViewController: UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return categorizedQuote.distinct.count + 1
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let index = indexPath.row
        
        switch index {
        case 0:
            let cell = tableView.dequeueReusableCell(withIdentifier: "CellHeaderQuoteList", for: indexPath) as! HeaderCellQuoteListTableViewCell
            cell.lblHeaderQuoteList.text = "Select Your Favourite Quote"
            cell.selectionStyle = .none
            return cell
        default:
            let cell = tableView.dequeueReusableCell(withIdentifier: "CellQuoteList", for: indexPath) as! QuoteListTableViewCell
            cell.imgArrow.image = UIImage(named: "arrow")
            cell.imgQuote.image = UIImage(named: categorizedQuote.distinct[index - 1])
            cell.lblCategory.text = "\(categorizedQuote.distinct[index - 1].uppercased())"
            cell.lblTotalQuote.text = "\(categorizedQuote.totalEach[index - 1]) quote(s)"
            return cell
        }
    }
}

// MARK: - TableView Delegate
extension QuoteListViewController: UITableViewDelegate{
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let indexPath = tableView.indexPathForSelectedRow
        
        if indexPath!.row == 0{
            return
        }else{
            let currentCell = tableView.cellForRow(at: indexPath!) as! QuoteListTableViewCell
            categoryToBePassing = currentCell.lblCategory.text!
            performSegue(withIdentifier: "goToListOfSelectedQuote", sender: self)
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch indexPath.row {
        case 0:
            return 37
        default:
            return 45
        }
    }
}



