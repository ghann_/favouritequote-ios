//
//  ContainerViewController.swift
//  FavouriteQuotes
//
//  Created by unrealBots on 9/11/17.
//  Copyright © 2017 Ghani. All rights reserved.
//

import UIKit
import SidebarOverlay

class ContainerViewController: SOContainerViewController {

    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .default
    }
    
    func setUpSidebarOverlay(){
        self.sideMenuWidth = 310
        self.menuSide = .left
        self.topViewController = self.storyboard?.instantiateViewController(withIdentifier: "HomePage")
        self.sideViewController = self.storyboard?.instantiateViewController(withIdentifier: "LeftScreen")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setUpSidebarOverlay()
    }
}
