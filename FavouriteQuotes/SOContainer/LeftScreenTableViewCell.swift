//
//  LeftScreenTableViewCell.swift
//  FavouriteQuotes
//
//  Created by unrealBots on 9/11/17.
//  Copyright © 2017 Ghani. All rights reserved.
//

import UIKit

class LeftScreenTableViewCell: UITableViewCell {

    @IBOutlet weak var lblLeftScreen: UILabel!
    @IBOutlet weak var imgLeftScreen: UIImageView!
    @IBOutlet weak var imgLeftScreenArrow: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

      
    }

}
