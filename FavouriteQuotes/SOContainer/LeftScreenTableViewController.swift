//
//  LeftScreenTableViewController.swift
//  FavouriteQuotes
//
//  Created by unrealBots on 9/11/17.
//  Copyright © 2017 Ghani. All rights reserved.
//

import UIKit

class LeftScreenViewController: UIViewController {
    let arrayMenu = ["Quotes", "Setting"]
    let arrayMenuImage = ["quote", "setting"]
}


// MARK: - TableView Data Source
extension LeftScreenViewController: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrayMenuImage.count + 1
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let index = indexPath.row
        
        switch index {
        case 0:
            let cell = tableView.dequeueReusableCell(withIdentifier: "LeftScreenMenuCell", for: indexPath) as! LeftScreenMenuTableViewCell
            cell.selectionStyle = .none
            cell.lblLeftScreenMenuScreen.text = "Menu"
            return cell
        default:
            let cell = tableView.dequeueReusableCell(withIdentifier: "LeftScreenCell", for: indexPath) as! LeftScreenTableViewCell
            cell.lblLeftScreen.text = arrayMenu[index-1]
            cell.imgLeftScreen.image = UIImage(named: arrayMenuImage[index-1])
            cell.imgLeftScreenArrow.image = UIImage(named: "arrow")
            return cell
        }
    }
}


// MARK: - TableView Delegate
extension LeftScreenViewController: UITableViewDelegate{
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let indexPath = tableView.indexPathForSelectedRow
        
        switch indexPath!.row {
        case 1:
            let profileViewController = self.storyboard!.instantiateViewController(withIdentifier: "HomePage")
            self.so_containerViewController?.topViewController = profileViewController
            break
        case 2:
            let profileViewController = self.storyboard!.instantiateViewController(withIdentifier: "SettingPage")
            self.so_containerViewController?.topViewController = profileViewController
            break
        default:
            break
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch indexPath.row {
        case 0:
            return 65
        default:
            return 40
        }
    }
}
