//
//  DetailFavouriteViewController.swift
//  FavouriteQuotes
//
//  Created by unrealBots on 9/11/17.
//  Copyright © 2017 Ghani. All rights reserved.
//

import UIKit
import HexColors

class DetailFavouriteViewController: UIViewController {

    var quoteText = ""
    var quoteAuthorText = ""
    
    @IBOutlet weak var lblQuoteAuthor: UILabel!
    @IBOutlet weak var lblQuote: UILabel!
    @IBOutlet weak var btnUnfavourite: UIButton!
    @IBOutlet weak var btnClose: UIButton!
    @IBOutlet weak var viewContainer: UIView!
    @IBAction func btnUnfavouriteTapped(_ sender: UIButton){
        //showAlert(titleText: "Alert", messageText: "Success Unfavorite")
    }
    @IBAction func btnCloseTapped(_ sender: UIButton){
        dismiss(animated: true, completion: nil)
    }
    
    func setUpButton(){
        viewContainer.layer.borderWidth = 1.3
        viewContainer.layer.cornerRadius = 4
        viewContainer.layer.borderColor = UIColor("#212F3C")?.cgColor
        viewContainer.alpha = 0.5
        
        btnUnfavourite.setTitle("Unfavourite", for: .normal)
        btnUnfavourite.setTitleColor(UIColor("#E74C3C"), for: .normal)
        btnUnfavourite.layer.cornerRadius = 4
        btnUnfavourite.layer.borderColor = UIColor("#E74C3C")?.cgColor
        btnUnfavourite.layer.borderWidth = 1.5
        
        btnClose.setTitle("Close", for: .normal)
        btnClose.setTitleColor(.white, for: .normal)
        btnClose.layer.cornerRadius = 4
        btnClose.layer.backgroundColor = UIColor("#E74C3C")?.cgColor
    }
}

// MARK: - Life Cycle
extension DetailFavouriteViewController{
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setUpButton()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        lblQuote.text = quoteText
        lblQuoteAuthor.text = quoteAuthorText
    }
}
