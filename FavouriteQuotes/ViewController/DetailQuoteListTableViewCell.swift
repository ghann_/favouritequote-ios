//
//  DetailQuoteListTableViewCell.swift
//  FavouriteQuotes
//
//  Created by unrealBots on 9/13/17.
//  Copyright © 2017 Ghani. All rights reserved.
//

import UIKit

class DetailQuoteListTableViewCell: UITableViewCell {

    @IBOutlet weak var lblNumberOfQuote: UILabel!
    @IBOutlet weak var lblQuoteText: UILabel!
    @IBOutlet weak var lblQuoteAuthor: UILabel!
    @IBOutlet weak var btnFavorite: UIButton!
    @IBAction func btnFavoriteTapped(_ sender: DetailQuoteListTableViewCell){
        if let cell = sender.superview?.superview as? DetailQuoteListTableViewCell{
            print(cell.lblQuoteText.text!)
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
