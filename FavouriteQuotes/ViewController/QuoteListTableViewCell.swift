//
//  QuoteListTableViewCell.swift
//  FavouriteQuotes
//
//  Created by unrealBots on 9/12/17.
//  Copyright © 2017 Ghani. All rights reserved.
//

import UIKit

class QuoteListTableViewCell: UITableViewCell {

    @IBOutlet weak var imgQuote: UIImageView!
    @IBOutlet weak var lblCategory: UILabel!
    @IBOutlet weak var lblTotalQuote: UILabel!
    @IBOutlet weak var imgArrow: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
