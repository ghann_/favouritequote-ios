//
//  RegisterViewController.swift
//  FavouriteQuotes
//
//  Created by unrealBots on 9/11/17.
//  Copyright © 2017 Ghani. All rights reserved.
//

import UIKit

class RegisterViewController: UIViewController {
    @IBAction func btnRegisterTapped(_ sender: UIButton){
        performSegue(withIdentifier: "goToFavouriteQuote", sender: self)
    }
}
