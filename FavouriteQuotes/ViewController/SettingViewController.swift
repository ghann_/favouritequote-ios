//
//  SettingViewController.swift
//  FavouriteQuotes
//
//  Created by unrealBots on 9/11/17.
//  Copyright © 2017 Ghani. All rights reserved.
//

import UIKit

class SettingViewController: UIViewController {

    @IBOutlet weak var swctAlternateBackground: UISwitch!
    @IBAction func swctChanged(_ sender: Any) {
        if swctAlternateBackground.isOn {
            print("On")
        } else {
            print("Off")
        }
    }
}


// MARK: - Life cycle
extension SettingViewController{
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        if let container = self.so_containerViewController {
            container.isSideViewControllerPresented = false
        }
    }
}
