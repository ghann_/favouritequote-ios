//
//  SignInViewController.swift
//  FavouriteQuotes
//
//  Created by unrealBots on 9/11/17.
//  Copyright © 2017 Ghani. All rights reserved.
//

import UIKit

class SignInViewController: UIViewController {
    @IBOutlet weak var txtUserNameField: UITextField!
    @IBOutlet weak var txtUserPasswordField: UITextField!
    @IBAction func btnSignInTapped(_ sender: UIButton){
        guard let userName = txtUserNameField.text, !userName.isEmpty else{
            //showAlert(titleText: "Alert", messageText: "Username can't be empty", actionTitleText: "Okay")
            return
        }
        guard let userPass = txtUserPasswordField.text, !userPass.isEmpty else{
            //showAlert(titleText: "Alert", messageText: "Userpassword can't be empty", actionTitleText: "Okay")
            return
        }

        switch sender.tag {
        case 0:
            processSignIn(user: userName, password: userPass)
        case 1:
            processSignInWithFireBase(user: userName, password: userPass)
        default:
            break
        }
    }
    
    @IBAction func btnRegisterTapped(_ sender: UIButton){
        performSegue(withIdentifier: "goToRegister", sender: self)
    }
    
    func processSignIn(user userName: String, password userPassword: String){
        print("Sign In")
    }
    func processSignInWithFireBase(user userName: String, password userPassword: String){
        print("Sign In with FireBase")
    }
    
}

extension SignInViewController{
    override func viewDidLoad() {
        
    }
}
