//
//  extension.swift
//  FavouriteQuotes
//
//  Created by unrealBots on 9/11/17.
//  Copyright © 2017 Ghani. All rights reserved.
//

import UIKit

extension UIViewController{
    func showAlert(titleText title: String, messageText message: String, actionTitleText actionTitle: String = "Ok", okHandler OkHandler: @escaping ((UIAlertAction) -> Void) ) {
        let alert = UIAlertController(title: title, message: "\n\(message)", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: actionTitle, style: .default, handler: OkHandler ))
        present(alert, animated: true, completion: nil)
    }
}
